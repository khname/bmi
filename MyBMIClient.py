import json

def calcBmi(weight, height):
    final_height = height/100
    final_height = final_height * final_height
    bmi =   weight/final_height
    bmi = round(bmi, 2)
    if bmi < 18.50:
        classification = 'underweight'
    elif bmi > 18.50 and bmi < 24.99:
        classification = 'normal range'
    elif bmi > 25.00 and bmi < 29.99:
        classification = 'overweight'
    else:
        classification = 'obese'
    return str(bmi) + ' ' + classification
final_bmi = calcBmi(103.2, 187)
print (final_bmi)

def listRanges():
    ranges_data = json.load(open('ranges.json'))    
    if(len(ranges_data['ranges'])) == 0:
        return 'UNDEFINED'
    else:
        return ranges_data['ranges']    
all_ranges = listRanges()
print(all_ranges)
