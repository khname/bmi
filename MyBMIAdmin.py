import json

def addCount() :
    count = json.load(open('count.json'))
    count["count"] = count["count"]+1
    with open('count.json', 'w') as outfile:
        json.dump(count, outfile)
        
def addRange(addCount, username, password, lower, upper, name, normal):
    addCount()
    json_data = json.load(open('users.json'))
    if username == json_data['username'] and password == json_data['password']:
        new_range = {
                "lower": lower,
                "upper": upper,
                "name": name,
                "normal": normal
            }
        ranges_data = json.load(open('ranges.json'))
        if(len(ranges_data['ranges'])) == 0:
            ranges_data['ranges'] = [new_range]
            with open('ranges.json', 'w') as outfile:
                json.dump(ranges_data, outfile)
        else:
            for my_range in ranges_data['ranges']:
                if (my_range['name'] == name):
                    return False
            ranges_data['ranges'].append(new_range)
            with open('ranges.json', 'w') as outfile:
                json.dump(ranges_data, outfile)
            return True
    else:
        return False
new_login = addRange(addCount, 'admin', 'bodymass', 50, 90, 'aaa', True)
print(new_login)

def deleteRange(addCount, username, password, range_name):
    addCount()
    json_data = json.load(open('users.json'))
    if username == json_data['username'] and password == json_data['password']:
        ranges_data = json.load(open('ranges.json'))   
        if(len(ranges_data['ranges'])) == 0:
            return False
        else:
            range_flag = 1
            index = 0
            for my_range in ranges_data['ranges']:
                if (my_range['name'] == range_name):
                    range_flag = 0
                    del ranges_data['ranges'][index]
                    break
                index += 1
            if range_flag == 0:
                with open('ranges.json', 'w') as outfile:
                    json.dump(ranges_data, outfile)
                return True
            else :
                return False
    else:
        return False


def callCount(username, password):
    json_data = json.load(open('users.json'))
    if username == json_data['username'] and password == json_data['password']:
        count = json.load(open('count.json'))
        return count["count"]
    else:
        return "-1"
print(callCount("admin", "bodymass"))
